package main

import (
	"./asm"
	"./size"
	"flag"
	"fmt"
	"os"
)

//var outFile = flag.String("o", "a.out", "output filename")
//var format = flag.String("t", "hex", "output format")

func main() {
	flag.Parse()
	var ret int

	// brauchen Unterkommando
	if flag.NArg() < 1 {
		usage()
		os.Exit(ret)
	}

	// entsprechendes Unterkommando aufrufen
	switch flag.Arg(0) {
	case "asm":
		ret = asm.Main()
	case "size":
		ret = size.Main()
	default:
		usage()
		ret = 1
	}

	if ret != 0 {
		os.Exit(ret)
	}
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: %s asm\n", os.Args[0])
}

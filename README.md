CSS: github2.css

# s43

s43 ist ein MSP430-Assembler.


### Nutzungsbeispiele

	s43 asm -o out.hex file1.s43 file2.s43
	s43 size out.hex

## asm

asm assembliert die übergebenen Dateien zu einer Intel HEX-Datei.

### Symbole

Symbole sind nur in der Datei definiert, in der sie stehen; ausser, wenn das erste Zeichen des Symbols ein Grossbuchstabe (A-Z) ist. Symbole dürfen auch UTF8-Zeichen enthalten.

### Direktiven

* `.asciiz`  
	Der darauf folgende String wird zusammen mit einem NUL-Byte
	in das aktuelle Segment aufgenommen.

* `.byte`  
	Alle Ausdrücke bis zum Zeilenende werden als Bytes interpretiert.

* `.endif`  
	Der entsprechende mit `.ifdef` eingeleitete Block wird geschlossen.

*  `.ifdef`  
	Alle bis zum entsprechenden `.endif` stehenden Anweisungen oder
	Daten werden dann ins aktuelle Segment aufgenommen,
	wenn das darauf folgende Symbol schon definiert ist.
  `.ifdef`s dürfen geschachtelt sein.

	Im folgenden Beispiel wird die Summe ignoriert,
	weil D erst später definiert ist.
	(in naken_asm würde die Summe nicht ignoriert werden):

		.ifdef D
		add #1, r15
		.endif

		.def D 42		; in naken_asm: .define

* `.seg`  
	Alle Anweisungen und Daten bis zum nächsten `.seg` bzw. Dateiende
	werden in einem Segment gesammelt.
	Dieses Segment ist über den darauf folgenden Ausdruck
	eindeutig identifizierbar und kann mehrfach verwendet werdet.
	Im Gegensatz zu `.org` in naken_asm wird der Location Counter dabei nicht
	zurückgesetzt.

		.seg RAM
		.space 42

		.seg ROM
		add #1, &0

		.seg RAM	; Fortsetzung des RAM-Segmentes
		.space 21

* `.space`  
  Der danach folgende Ausdruck reserviert Speicherplatz; der Ausdruck darf
  kein Symbol enthalten, dessen Wert zu dem Zeitpunkt noch nicht bekannt ist,
  weil sich sonst eine gegenseitige Abhängigkeit ergibt:
  `.space` beeinflusst das Symbol und das Symbol beeinflusst `.space`.

* `.word`  
  Alle Ausdrücke bis zum Zeilenende werden als Worte interpretiert.

### Intern

* Der Lexer bekommt eine Folge von Bytes und extrahiert daraus Tokens;
  dabei werden Sonderzeichen (alles ausser [0-9A-Za-z\_] als Trennzeichen
  der Tokens angesehen; alle Trennzeichen ausser Leerzeichen und Tab werden an
  den Parser weitergereicht; Kommentare werden vom Lexer übersprungen und nicht
  an den Parser weitergereicht

* Obwohl ich zunächst versucht habe `.ifdef` und `.endif` nicht zu implementieren, musste ich es schliesslich doch tun, z.B. um Hardware zu initialisieren, die es nur bei den groesseren MSPs gibt.

		.ifdef P3OUT
		...
		.endif

	Dies ist so umgesetzt, dass die if-Bloecke in ein negatives Segment geparst werden, dieses dann aber nicht ausgegeben wird.

### Bugs

* *32768 exceeds word size*  
  Bei Verwendung der Konstante -32768 wird obige Fehlermeldung erzeugt. Dieser Wert passt zwar in ein 16Bit-Wort, erzeugt aber trotzdem eine Fehlermeldung, weil der Parser zuerst die 32768 sieht (was nicht mehr in ein vorzeichenbehaftetes 16Bit-Wort passt) und erst dann das Minuszeichen. Der erzeugte Objektcode ist aber trotzdem richtig. Ich weiss nicht, wie man die Fehlermeldung unterbinden kann (der Parser kann nicht in die Zukunft sehen und weiss nicht, ob noch ein Minuszeichen kommt).

## size
size gibt die Größe jeder übergebenen Intel HEX-Datei aus.


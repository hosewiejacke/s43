/* http://dinosaur.compilertools.net/yacc/
   https://github.com/golang-samples/yacc/blob/master/simple/calc.y
   http://stackoverflow.com/questions/8422146/go-how-to-create-a-parser */

%{
package asm

import (
)

type oper struct {
	mode, reg int
	expr *expr // kein int, weil wir wissen muessen, ob future symbol dabei
			   // Zeiger, weil expr auch nil sein kann
}

// expr ist ein struct und kein int, weil wir wissen muessen, ob bei der
// Evaluation ein future symbol verwendet wurde; falls das der Fall ist,
// duerfen wir keinen CG verwenden und nicht als Parameter in .space
type expr struct {
	n int
	p int // absolute position des Ausdrucks in allen Quelldateien
}

%}

/* Typen, die im Keller sein koennen */
%union {
	oper *oper			// Operand (Quelle oder Ziel)
	expr *expr
	exprs []*expr

	string string
	int int
	ints []int
}

%start S

/* Typen, die der Lexer mit Wert zurueckgeben kann */
%token <int> MNEM2 MNEM1 JMP RETI REGISTER NUMBER
%token <string> STRING

/* Typen, die der Lexer ohne Wert zurueckgeben kann */
%token DEF IFDEF ENDIF
%token BYTE WORD SPACE ASCIIZ
%token SEG
%token NONTERMINATED_STRING NONTERMINATED_COMMENT UNKNOWN_DIRECTIVE STRANGE_RUNE

/* Typen, die die Produktionsregeln zurueckgeben */
%type <oper> src dest
%type <exprs> exprs
%type <expr> expr

/* Rangordnung (aufsteigend) */
%left '+' '-' '|' '^'
%left '*' '/' '%' '&'
%right UNARY /* imaginaerer rechtassoziative operator */

%%

/* Grossgeschriebene Worte sind Terminale, die der Lexer zurueckgibt (s. o.);
   kleingeschriebene sind Produktionsregeln */

S		:
		| S eol

		/* Anweisung mit und ohne Label */
		| S instr eol
		| S label ':' instr eol

		/* nur Label */
		| S label ':' eol

		/* Direktiven */
		| S label ':' dir eol
		| S dir eol

label	: STRING					{ label($1) }
		;
instr	: MNEM2	src ',' dest		{ mnem2($1, $2, $4) }
		| MNEM1 src					{ mnem1($1, $2) }
		| RETI						{ reti($1) }
		| JMP expr					{ jmp($1, $2) }
		;
dir		: DEF STRING expr			{ def($2, $3) }
		| DEF STRING				{ def($2, &expr{n: 1, p: pos}) }
		| IFDEF STRING				{ ifdef($2) }
		| ENDIF						{ endif() }

		| BYTE exprs				{ bite($2...) }
		| WORD exprs				{ word($2...) }
		| SPACE expr				{ space($2) }
		| ASCIIZ STRING				{ asciiz($2) }

		| SEG expr					{ seg = $2.n } /* future ref wohl ok */
		;
eol		: '\n'						{ pos++; line++ }
		;
src		: REGISTER					{ $$ = &oper{mode: 0, expr: nil, reg: $1} }
		| expr '(' REGISTER ')'		{ $$ = &oper{mode: 1, expr: $1, reg: $3}}
		| expr						{ $$ = &oper{mode: 1, expr: $1, reg: 0} }
		| '&' expr					{ $$ = &oper{mode: 1, expr: $2, reg: 2} }
		| '@' REGISTER				{ $$ = &oper{mode: 2, expr: nil, reg: $2} }
		| '@' REGISTER '+'			{ $$ = &oper{mode: 3, expr: nil, reg: $2} }
		| '#' expr					{ $$ = &oper{mode: 3, expr: $2, reg: 0} }
		;
dest	: REGISTER					{ $$ = &oper{mode: 0, expr: nil, reg: $1} }
		| expr '(' REGISTER ')'		{ $$ = &oper{mode: 1, expr: $1, reg: $3}}
		| expr						{ $$ = &oper{mode: 1, expr: $1, reg: 0} }
		| '&' expr					{ $$ = &oper{mode: 1, expr: $2, reg: 2} }
		;
exprs	: exprs ',' expr			{ $$ = append($1, $3) }
		| expr						{ $$ = []*expr{$1} }
		;
expr	: expr '+' expr				{ $$ = calc($1, '+', $3) }
		| expr '-' expr				{ $$ = calc($1, '-', $3) }
		| expr '|' expr				{ $$ = calc($1, '|', $3) }
		| expr '^' expr				{ $$ = calc($1, '^', $3) }

		| expr '*' expr				{ $$ = calc($1, '*', $3) }
		| expr '/' expr				{ $$ = calc($1, '/', $3) }
		| expr '%' expr				{ $$ = calc($1, '%', $3) }
		| expr '&' expr				{ $$ = calc($1, '&', $3) }

		| '(' expr ')'				{ $$ = $2 }

		| '+' expr %prec UNARY		{ $$ = $2 }
		| '-' expr %prec UNARY		{ $$ = $2; $$.n = -$$.n }
		| '^' expr %prec UNARY		{ $$ = $2; $$.n ^= -1 }

		| NUMBER					{ $$ = number($1) }
		| STRING					{ $$ = symbol($1) }
		;

%%

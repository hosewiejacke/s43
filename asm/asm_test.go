package asm

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

func TestCG2(t *testing.T) {
	// CG bei mnem2
	Read(strings.NewReader(`
mov #-1, r4
mov #0, r4
mov #1, r4
mov #2, r4
mov #4, r4
mov #8, r4
	`))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	if len(obj[0]) != 12 {
		t.Errorf("object has wrong size")
	}

	if obj[0][1] != byte(0x43) || obj[0][0] != byte(0x34) {
		t.Errorf("-1 wrong: %x%x", obj[0][1], obj[0][0])
	}
	if obj[0][3] != byte(0x43) || obj[0][2] != byte(0x04) {
		t.Errorf("0 wrong: %x%x", obj[0][3], obj[0][2])
	}
	if obj[0][5] != byte(0x43) || obj[0][4] != byte(0x14) {
		t.Errorf("1 wrong: %x%x", obj[0][5], obj[0][4])
	}
	if obj[0][7] != byte(0x43) || obj[0][6] != byte(0x24) {
		t.Errorf("2 wrong: %x%x", obj[0][7], obj[0][6])
	}
	if obj[0][9] != byte(0x42) || obj[0][8] != byte(0x24) {
		t.Errorf("4 wrong: %x%x", obj[0][9], obj[0][8])
	}
	if obj[0][11] != byte(0x42) || obj[0][10] != byte(0x34) {
		t.Errorf("8 wrong: %x%x", obj[0][11], obj[0][10])
	}
}

func TestCG1(t *testing.T) {
	// CG bei mnem1, aber nicht mit push, weil dort Sandfehler
	Read(strings.NewReader(`
rrc #-1
rrc #0
rrc #1
rrc #2
rrc #4
rrc #8
	`))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	if len(obj[0]) != 12 {
		t.Errorf("object has wrong size: %d", len(obj[0]))
	}

	if obj[0][1] != byte(0x10) || obj[0][0] != byte(0x33) {
		t.Errorf("-1 wrong: %x%x", obj[0][1], obj[0][0])
	}
	if obj[0][3] != byte(0x10) || obj[0][2] != byte(0x03) {
		t.Errorf("0 wrong: %x%x", obj[0][3], obj[0][2])
	}
	if obj[0][5] != byte(0x10) || obj[0][4] != byte(0x13) {
		t.Errorf("1 wrong: %x%x", obj[0][5], obj[0][4])
	}
	if obj[0][7] != byte(0x10) || obj[0][6] != byte(0x23) {
		t.Errorf("2 wrong: %x%x", obj[0][7], obj[0][6])
	}
	if obj[0][9] != byte(0x10) || obj[0][8] != byte(0x22) {
		t.Errorf("4 wrong: %x%x", obj[0][9], obj[0][8])
	}
	if obj[0][11] != byte(0x10) || obj[0][10] != byte(0x32) {
		t.Errorf("8 wrong: %x%x", obj[0][11], obj[0][10])
	}
}

func TestCGBug(t *testing.T) {
	// bei 'push #4' und 'push #8' kein CG verwenden
	Read(strings.NewReader(`
push #-1
push #0
push #1
push #2
push #4
push #8
	`))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	if len(obj[0]) != 16 {
		t.Errorf("object has wrong size: %d", len(obj[0]))
	}

	if obj[0][1] != byte(0x12) || obj[0][0] != byte(0x33) {
		t.Errorf("-1 wrong: %x%x", obj[0][1], obj[0][0])
	}
	if obj[0][3] != byte(0x12) || obj[0][2] != byte(0x03) {
		t.Errorf("0 wrong: %x%x", obj[0][3], obj[0][2])
	}
	if obj[0][5] != byte(0x12) || obj[0][4] != byte(0x13) {
		t.Errorf("1 wrong: %x%x", obj[0][5], obj[0][4])
	}
	if obj[0][7] != byte(0x12) || obj[0][6] != byte(0x23) {
		t.Errorf("2 wrong: %x%x", obj[0][7], obj[0][6])
	}
	if obj[0][9] != byte(0x12) || obj[0][8] != byte(0x30) ||
		obj[0][11] != byte(0x00) || obj[0][10] != byte(0x04) {
		t.Errorf("4 wrong: %x%x%x%x", obj[0][9], obj[0][8], obj[0][11], obj[0][10])
	}
	if obj[0][13] != byte(0x12) || obj[0][12] != byte(0x30) ||
		obj[0][15] != byte(0x00) || obj[0][14] != byte(0x08) {
		t.Errorf("8 wrong: %x%x%x%x", obj[0][13], obj[0][12], obj[0][15], obj[0][14])
	}
}

func TestSegments(t *testing.T) {
	Read(strings.NewReader(`
;.seg 0 implizit
.byte 0, 1

.seg 1
.byte 10, 11

.seg 2
.byte 20, 21

.seg 0
.byte 2

.seg 1
.byte 12

.seg 2
.byte 22
	`))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	if len(obj) == 0 {
		t.Errorf("object is empty")
	}

	for i, seg := range obj {
		if len(obj) == 0 {
			t.Errorf("object is empty")
		}

		for j, b := range seg {
			if b != byte(i*10+j) {
				t.Errorf("wrong '%x' in section %d at offset %d", b, i, j)
			}
		}
	}
}

func TestBytes(t *testing.T) {
	bites := []struct {
		i int
		b []byte
	}{
		{-128, []byte{0x80}},
		{-127, []byte{0x81}},
		{-126, []byte{0x82}},
		{-3, []byte{0xfd}},
		{-2, []byte{0xfe}},
		{-1, []byte{0xff}},
		{0, []byte{0x00}},
		{1, []byte{0x01}},
		{2, []byte{0x02}},
		{3, []byte{0x03}},
		{126, []byte{0x7e}},
		{127, []byte{0x7f}},
	}

	// Eingabe
	var buf bytes.Buffer
	wr := bufio.NewWriter(&buf)
	for _, bite := range bites {
		fmt.Fprintf(wr, ".byte %d\n", bite.i)
	}
	if err := wr.Flush(); err != nil {
		t.Errorf("%v", err)
	}
	Read(bufio.NewReader(&buf))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	off := 0
	for _, bite := range bites {
		b := obj[0][off : off+1]

		if bytes.Compare(b, bite.b) != 0 {
			t.Errorf("word %d ist % x instead of % x", bite.i, b, bite.b)
		}
		off += 1
	}
}

func TestWords(t *testing.T) {
	words := []struct {
		i int
		b []byte
	}{
		//{-32768, []byte{0x00, 0x80}},
		{-257, []byte{0xff, 0xfe}},
		{-256, []byte{0x00, 0xff}},
		{-255, []byte{0x01, 0xff}},
		{-254, []byte{0x02, 0xff}},
		{-129, []byte{0x7f, 0xff}},
		{-128, []byte{0x80, 0xff}},
		{-127, []byte{0x81, 0xff}},
		{-126, []byte{0x82, 0xff}},
		{-3, []byte{0xfd, 0xff}},
		{-2, []byte{0xfe, 0xff}},
		{-1, []byte{0xff, 0xff}},
		{0, []byte{0x00, 0x00}},
		{1, []byte{0x01, 0x00}},
		{2, []byte{0x02, 0x00}},
		{3, []byte{0x03, 0x00}},
		{126, []byte{0x7e, 0x00}},
		{127, []byte{0x7f, 0x00}},
		{128, []byte{0x80, 0x00}},
		{129, []byte{0x81, 0x00}},
		{254, []byte{0xfe, 0x00}},
		{255, []byte{0xff, 0x00}},
		{256, []byte{0x00, 0x01}},
		{257, []byte{0x01, 0x01}},
		{32767, []byte{0xff, 0x7f}},
	}

	var buf bytes.Buffer

	wr := bufio.NewWriter(&buf)
	for _, w := range words {
		fmt.Fprintf(wr, ".word %d\n", w.i)
	}
	if err := wr.Flush(); err != nil {
		t.Errorf("%v", err)
	}

	Read(bufio.NewReader(&buf))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	off := 0
	for _, w := range words {
		b := obj[0][off : off+2]

		if bytes.Compare(b, w.b) != 0 {
			t.Errorf("word %d is % x instead of % x", w.i, b, w.b)
		}
		off += 2
	}
}

func TestSpace(t *testing.T) {
	Read(strings.NewReader(`
.space 5
	`))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	if bytes.Compare(obj[0], []byte{0, 0, 0, 0, 0}) != 0 {
		t.Errorf(".space did not allocate 5 empty bytes")
	}
}

func TestExpr(t *testing.T) {
	exprs := []struct {
		e string
		b []byte
	}{
		{"-1 + 1", []byte{0x00, 0x00}},
		{"1 * 2", []byte{0x02, 0x00}},
		{"l4 + l6", []byte{0x0a, 0x00}}, // future symbol
		{"1-2+3-5", []byte{0xfd, 0xff}},
		{"-(1+2)*4", []byte{0xf4, 0xff}},
	}

	var buf bytes.Buffer

	wr := bufio.NewWriter(&buf)
	for i, e := range exprs {
		fmt.Fprintf(wr, "l%d: .word %s\n", i*2, e.e) // *2, weil Wort
		//fmt.Printf("l%d: .word %s\n", i*2, e.e)      // *2, weil Wort
	}
	if err := wr.Flush(); err != nil {
		t.Errorf("%v", err)
	}
	Read(bufio.NewReader(&buf))
	Write(bufio.NewWriter(new(bytes.Buffer)), "hex")

	for i, e := range exprs {
		b := obj[0][i*2 : i*2+2]

		if bytes.Compare(b, e.b) != 0 {
			t.Errorf("expr \"%s\" evaluated as % x instead of % x", e.e, b, e.b)
		}
	}
}

func TestExpr2(t *testing.T) {
	// Ausdruecke mit Symbolen
	Read(strings.NewReader(`
.def d 0x40
	.word 0
l:	mov #d+l, r4
	`))
	Write(bufio.NewWriter(ioutil.Discard), "hex")

	if len(obj[0]) != 6 {
		t.Errorf("obj has wrong size: %d", len(obj[0]))
	}
	if obj[0][4] != byte(0x42) || obj[0][5] != byte(0) {
		t.Errorf("expr evaluated as '% x' instead of '42 00'", obj[0][4:6])
	}
}

func TestDef1(t *testing.T) {
	// define ohne Ausdruck muss Wert auf 1 setzen

	Read(strings.NewReader(`
.def hallo
.byte hallo
	`))
	Write(bufio.NewWriter(ioutil.Discard), "hex")

	if len(obj) != 1 || obj[0][0] != byte(1) {
		t.Errorf("empty define did not evaluate to 1")
	}
}

/*	aus irgendeinem Grund belaessen 2 foldende Test Reste in obj,
	was zum Fehlschlag anderer Test fuehrt
func TestDef2(t *testing.T) {
	// define mit future label darf nicht 0 zurueckgeben

	Read(strings.NewReader(`
.def hallo l0
l0: .byte 0
	`))
	if Write(bufio.NewWriter(ioutil.Discard), "hex") == 0 {
		t.Errorf("define with future label not did not fail")
	}
}

func TestDef3(t *testing.T) {
	// define mit future define darf nicht 0 zurueckgeben

	Read(strings.NewReader(`
.def hallo welt
.def welt  0
	`))
	if Write(bufio.NewWriter(ioutil.Discard), "hex") == 0 {
		t.Errorf("define with future define not did not fail")
	}
}
*/
func TestIfDefSimple(t *testing.T) {
	// einfache ifdefs
	Read(strings.NewReader(`
.def hallo 1
.ifdef hallo
.byte 21
.endif
	`))
	Write(bufio.NewWriter(ioutil.Discard), "hex")
	if len(obj[0]) != 1 || obj[0][0] != byte(21) {
		t.Errorf(".ifdef assembled to '% x'", obj[0])
	}
}

func TestIfDefNested(t *testing.T) {
	// geschachtelte ifdefs
	Read(strings.NewReader(`
.seg 2
.def ja

.ifdef ja
	.byte 1
	.ifdef nein
		.byte 2
	.endif
	.byte 3
.endif
	`))
	Write(bufio.NewWriter(ioutil.Discard), "hex")
	if len(obj[2]) != 2 || obj[2][0] != byte(1) || obj[2][1] != byte(3) {
		t.Errorf(".ifdef assembled to '% x'", obj[2])
	}
}

func TestMn2(t *testing.T) {
	// Opcodes von 2-Operand-Anweisungen
	Read(strings.NewReader(`
mov #3, r4
add #3, r4
addc #3, r4
subc #3, r4
sub #3, r4
cmp #3, r4
dadd #3, r4
bit #3, r4
bic #3, r4
bis #3, r4
xor #3, r4
and #3, r4
	`))
	Write(bufio.NewWriter(ioutil.Discard), "hex")

	if len(obj[0]) != 48 { // 12 instr * 2 Worte + 12 immediates a 2 Worte
		t.Errorf("obj has wrong size %d", len(obj[0]))
	}

	// mn2 haben aufsteigende opcodes im MSN des MSB
	for i := 0; i < 12; i++ {
		if int(obj[0][i*4+1])&0xf0 != 0x40+i*0x10 {
			t.Errorf("wrong opcode in line %d: % x", i+1, obj[0][i*4+1])
		}
	}
}

func TestMn2B(t *testing.T) {
	// Opcodes von 2-Operand-Byteanweisungen
	Read(strings.NewReader(`
mov.b #3, r4
add.b #3, r4
addc.b #3, r4
subc.b #3, r4
sub.b #3, r4
cmp.b #3, r4
dadd.b #3, r4
bit.b #3, r4
bic.b #3, r4
bis.b #3, r4
xor.b #3, r4
and.b #3, r4
	`))
	Write(bufio.NewWriter(ioutil.Discard), "hex")

	if len(obj[0]) != 48 { // 12 instr * 2 Worte + 12 immediates a 2 Worte
		t.Errorf("obj has wrong size %d", len(obj[0]))
	}

	// mn2 haben aufsteigende opcodes im MSN des MSB und Bit6 im LSB
	for i := 0; i < 12; i++ {
		if int(obj[0][i*4+1])&0xf0 != 0x40+i*0x10 ||
			int(obj[0][i*4])&0x40 != 0x40 {
			t.Errorf("wrong opcode in line %d: % x", i+1, obj[0][i*4+1])
		}
	}
}

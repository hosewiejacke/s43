package asm

import (
	"strconv"
	"unicode/utf8"
)

var opcodes = map[string]int{
	// mnem2
	"mov":  0x4000,
	"add":  0x5000,
	"addc": 0x6000,
	"subc": 0x7000,
	"sub":  0x8000,
	"cmp":  0x9000,
	"dadd": 0xa000,
	"bit":  0xb000,
	"bic":  0xc000,
	"bis":  0xd000,
	"xor":  0xe000,
	"and":  0xf000,

	"mov.b":  0x4040,
	"add.b":  0x5040,
	"addc.b": 0x6040,
	"subc.b": 0x7040,
	"sub.b":  0x8040,
	"cmp.b":  0x9040,
	"dadd.b": 0xa040,
	"bit.b":  0xb040,
	"bic.b":  0xc040,
	"bis.b":  0xd040,
	"xor.b":  0xe040,
	"and.b":  0xf040,

	// mnem1
	"rrc":  0x1000,
	"swpb": 0x1080,
	"rra":  0x1100,
	"sxt":  0x1180,
	"push": 0x1200,
	"call": 0x1280,

	"rrc.b":  0x1040,
	"rra.b":  0x1140,
	"push.b": 0x1240,

	// "reti":
	"reti": 0x1300,

	// "jmp":
	"jne": 0x2000,
	"jnz": 0x2000,
	"jeq": 0x2400,
	"jz":  0x2400,
	"jnc": 0x2800,
	"jc":  0x2c00,
	"jn":  0x3000,
	"jge": 0x3400,
	"jl":  0x3800,
	"jmp": 0x3c00,
}

var reg = map[string]int{
	"pc": 0, "PC": 0,
	"sp": 1, "SP": 1,
	"sr": 2, "SR": 2,
	"cg": 3, "CG": 3,

	"r0": 0, "R0": 0,
	"r1": 1, "R1": 1,
	"r2": 2, "R2": 2,
	"r3": 3, "R3": 3,
	"r4": 4, "R4": 4,
	"r5": 5, "R5": 5,
	"r6": 6, "R6": 6,
	"r7": 7, "R7": 7,
	"r8": 8, "R8": 8,
	"r9": 9, "R9": 9,
	"r10": 10, "R10": 10,
	"r11": 11, "R11": 11,
	"r12": 12, "R12": 12,
	"r13": 13, "R13": 13,
	"r14": 14, "R14": 14,
	"r15": 15, "R15": 15,
}

// lexer muss Lex() und Error() implementieren, um vom Parser genutzt zu werden
type lexer struct {
}

func (l *lexer) Lex(lval *yySymType) int {

	begin, end := 0, 0 // Anfang und Ende des Tokens
	var r rune
	var size int

skipBlanks:
	// Token faengt erst nach fuehrenden Leerzeichen an
	for size = 0; begin < len(src); begin += size {
		r, size = utf8.DecodeRune(src[begin:])
		switch r {
		case ' ', '\t':
			// immer noch Blanks
		default:
			break skipBlanks
		}
	}

	// wenn Token mit ';' / '"' anfaengt, dann bis '"' / '\n' ueberspringen
	// bei einigen Zeichen am Anfang endet das Token nicht regulaer
	r, size = utf8.DecodeRune(src[begin:])
	switch r {
	case ';': // wenn Token mit ';' anfaengt, dann ist es ein Kommentar bis \n
		for size = 0; begin < len(src); begin += size {
			r, size = utf8.DecodeRune(src[begin:])
			if r == '\n' {
				src = src[begin+size:]
				return '\n' // um Regeln nicht zu verletzen
			}
		}
		return NONTERMINATED_COMMENT // kein abschliessendes '\n'
	case '"': // wenn Token mit '"' anfaengt, dann ist es ein String bis '"'
		begin += 1 // '"' ueberspringen
		for size, end = 0, begin; end < len(src); end += size {
			r, size = utf8.DecodeRune(src[end:])
			if r == '"' {
				lval.string = string(src[begin:end])
				src = src[end+size:] // nach '"' weitermachen
				return STRING
			}
		}
		return NONTERMINATED_STRING // keines abschliessendes '"'
	case '\'':
		if len(src[begin:]) >= 3 {
			if src[begin+2] == '\'' { // mit ' abgeschlossen
				lval.int = int(src[begin+1])
				src = src[begin+3:]
				return NUMBER
			} else {
				src = src[begin+3:] // macht Rest nicht besser...
				return STRANGE_RUNE
			}
		} // sonst: kein '.' => erstmal weitergucken
	}

	// sonst hoert Token bei einem Trennzeichen auf
	for size, end = 0, begin; end < len(src); end += size {
		r, size = utf8.DecodeRune(src[end:])

		if 'a' <= r && r <= 'z' ||
			'0' <= r && r <= '9' ||
			'A' <= r && r <= 'Z' ||
			r == '_' || r == '.' || r > 0xff {
			continue
		} else {
			// Trennzeichen
			if begin == end { // Trennzeichen am Anfang ist eingenstaend. Token
				src = src[end+size:] // *nach* Zeichen weitermachen
				return int(r)
			}
			// Trennzeichen nicht am Anfang
			break // aufhoeren und Token deuten
		}
	}

	// Ende des Tokens jetzt auch bekannt
	token := src[begin:end]
	src = src[end:]

	// Token deuten
	if n, err := strconv.ParseInt(string(token), 0, strconv.IntSize); err == nil {
		lval.int = int(n)
		return NUMBER
	} else if r, ok := reg[string(token)]; ok {
		lval.int = r
		return REGISTER
	} else if op, ok := opcodes[string(token)]; ok {
		lval.int = op
		switch {
		case 0x4000 <= op:
			return MNEM2
		case 0x2000 <= op && op <= 0x3cff:
			return JMP
		case op == 0x1000 || op == 0x1040 || op == 0x1080 ||
			op == 0x1100 || op == 0x1140 || op == 0x1180 ||
			op == 0x1200 || op == 0x1240 || op == 0x1280:
			return MNEM1
		case op == 0x1300:
			return RETI
		default:
			panic("strange opcode")
		}
	} else if len(token) > 0 {
		if token[0] != '.' {
			lval.string = string(token)
			return STRING
		} else {
			switch string(token) {
			case ".def":
				return DEF
			case ".ifdef":
				return IFDEF
			case ".endif":
				return ENDIF

			case ".byte":
				return BYTE
			case ".word":
				return WORD
			case ".space":
				return SPACE
			case ".asciiz":
				return ASCIIZ

			case ".seg":
				return SEG
			default:
				//fmt.Printf("komische dir: %s\n", token)
				return UNKNOWN_DIRECTIVE
			}
		}
	} else if len(src) == 0 {
		return 0 // EOF, da Token und Quelltext Laenge 0 haben
	}

	// diese Stelle sollten wir nie erreichen
	panic("could not interpret token")
}

func (l *lexer) Error(s string) {
	warn(1, "%v", s)
}

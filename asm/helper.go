package asm

import (
	"bytes"
	"fmt"
	"os"
	"strconv"
)

func label(s string) {
	lc := seg + len(obj[seg]) // Anfang + Laenge des Segments

	if e, ok := symTab[s]; !ok { // Label noch nicht vergeben
		symTab[s] = expr{n: lc, p: pos} // Label mit LC definieren
	} else if e.n == lc { // gleiche Adresse (aus 1. Durchlauf)
	} else {
		warn(1, "%s redefined (%d -> %d)", s, e.n, lc)
		warn(2, "%s redefined (%d -> %d)", s, e.n, lc)
	}
}

// mnem2 erstellt aus Opcode und 2 Operanden eine Anweisung
// und fuegt sie dem aktuellen Segment hinzu
func mnem2(opc int, s, d *oper) {
	lc := seg + len(obj[seg])
	if lc&1 == 1 {
		warn(1, "instruction starts at odd address")
	}

	if s.reg == 0 && s.mode == 3 { // unmittelbare Quelle
		if s.expr.p <= pos { // CG nur benutzen, wenn keine future ref
			// wenn CG benutzt wird, dann expr auf nil setzen
			switch s.expr.n {
			case -1:
				s.reg, s.mode, s.expr = 3, 3, nil
			case 0:
				s.reg, s.mode, s.expr = 3, 0, nil
			case 1:
				s.reg, s.mode, s.expr = 3, 1, nil
			case 2:
				s.reg, s.mode, s.expr = 3, 2, nil
			case 4:
				s.reg, s.mode, s.expr = 2, 2, nil
			case 8:
				s.reg, s.mode, s.expr = 2, 3, nil
			}
		}

		// bei Byteanweisungen (mit unmittelbarer Quelle) MSB auf 0 setzen
		if opc&0x0040 != 0 && s.expr != nil {
			if s.expr.n < -128 || 255 < s.expr.n {
				warn(2, "%d exceeds byte range")
			}
			s.expr.n &= 0xff
		}
	} else if s.reg == 0 && s.mode == 1 { // symbolische Quelle
		s.expr.n -= (lc + 2) // Ausdruck muss relativ zum PC sein
	}

	if d.reg == 0 && d.mode == 1 { // symbolisches Ziel
		// Ausdruck muss relativ zum PC sein
		if s.expr != nil {
			d.expr.n -= (lc + 4) // nach Anweisung von "externe" Quelle
		} else {
			d.expr.n -= (lc + 2)
		}
	}

	// Sachen mit Opcode vereinigen und an Objekt anhaengen
	opc |= s.reg<<8 | d.mode<<7 | s.mode<<4 | d.reg
	obj[seg] = append(obj[seg], byte(opc), byte(opc>>8))

	// Quellwert, der nicht in Opcode passt
	if s.expr != nil {
		obj[seg] = append(obj[seg], byte(s.expr.n), byte(s.expr.n>>8))
	}

	// Zielwert, der nicht in Opcode passt
	if d.expr != nil {
		obj[seg] = append(obj[seg], byte(d.expr.n), byte(d.expr.n>>8))
	}
}

// mnem1 erstellt aus Opcode und Operand eine Anweisung
// und fuegt sie dem aktuellen Segment hinzu
func mnem1(opc int, d *oper) {
	lc := seg + len(obj[seg])
	if lc&1 == 1 {
		warn(1, "instruction starts at odd address")
	}

	if d.reg == 0 && d.mode == 3 { // unmittelbares Ziel
		if d.expr.p <= pos { // wenn CG benutzt wird, dann expr auf nil setzen
			switch d.expr.n {
			case -1:
				d.reg, d.mode, d.expr = 3, 3, nil
			case 0:
				d.reg, d.mode, d.expr = 3, 0, nil
			case 1:
				d.reg, d.mode, d.expr = 3, 1, nil
			case 2:
				d.reg, d.mode, d.expr = 3, 2, nil
			case 4:
				if opc != 0x1200 && opc != 0x1240 { // kein 'push[.b] #4'
					d.reg, d.mode, d.expr = 2, 2, nil
				}
			case 8:
				if opc != 0x1200 && opc != 0x1240 { // kein 'push[.b] #8'
					d.reg, d.mode, d.expr = 2, 3, nil
				}
			}
		}

		// bei Byteanweisungen (mit unmittelbarem Wert) MSB auf 0 setzen
		if opc&0x0040 != 0 && d.expr != nil {
			if d.expr.n < -128 || 255 < d.expr.n {
				warn(2, "%d exceeds byte range")
			}
			d.expr.n &= 0xff
		}
	} else if d.reg == 0 && d.mode == 1 { // symbolisches Ziel
		// Ausdruck muss relativ zum PC sein
		d.expr.n -= (lc + 2)
	}

	opc |= d.mode<<4 | d.reg
	obj[seg] = append(obj[seg], byte(opc), byte(opc>>8))

	// Zielwert, der nicht in Opcode passt
	if d.expr != nil {
		obj[seg] = append(obj[seg], byte(d.expr.n), byte(d.expr.n>>8))
	}
}

func reti(opc int) {
	if (seg+len(obj[seg]))&1 == 1 {
		warn(1, "instruction starts at odd address")
	}
	obj[seg] = append(obj[seg], byte(opc), byte(opc>>8))
}

func jmp(opc int, dest *expr) {
	lc := seg + len(obj[seg])
	if lc&1 == 1 {
		warn(1, "instruction starts at odd address")
	}

	// Ziel sollte gerade sein
	if dest.n&1 == 1 {
		warn(2, "jump destination is odd")
	}

	// Offset wird in Worten angegeben und darf 9 bit nicht ueberschreiten
	off := (dest.n - lc - 2) / 2 // -2, weil nach Lesen zeigt PC auf naechste
	if off < -512 || 511 < off {
		warn(2, "jump destination pc%+d exceeds range", off)
	}

	// Offset auf 9 bits beschraenken, mit Opcode vereinigen und zurueckgeben
	opc |= off & 0x3ff
	obj[seg] = append(obj[seg], byte(opc), byte(opc>>8))
}

// define fuegt symbol in symTab ein
func def(s string, e *expr) {
	if e.p > pos { // future refs nicht erlaubt
		warn(1, "cannot use a future reference")
	}

	if ex, ok := symTab[s]; !ok { // noch nicht definiert
		symTab[s] = *e
	} else if ex.n == e.n { // wohl im 1. Durchlauf gleich definiert
	} else { // anders definiert
		warn(1, "%s redefined (%d -> %d)", s, ex.n, e.n)
		warn(2, "%s redefined (%d -> %d)", s, ex.n, e.n)
	}
}

// ifdef setzt Segment auf negativen Wert, wenn Symbol nicht definiert ist
func ifdef(s string) {
	// Adresse des aktuellen Segments in ifDepth sichern;
	// len(ifDepth) ist Verschachtelungstiefe
	ifStack = append(ifStack, seg)

	// wenn Symbol nicht definiert ist, dann wird der Code in einem negativen
	// Segment gesammelt, das dann nicht ausgegeben wird;
	// Symbol, das erst in Zukunft definiert wird, gilt als undefiniert, weil
	// der LC sonst an einer Stelle im 1. und 2. Durchlauf verschiedene Werte
	// annimmt
	if e, ok := symTab[s]; !ok || e.p > pos {
		seg = -2 // irgendeine negative gerade Zahl
	}
	// sonst: Symbol definiert => einfach weitermachen
}

func endif() {
	// sicherstellen, dass vorher ein ifdef war
	len := len(ifStack)
	if len == 0 {
		warn(1, ".endif hat kein zugehoeriges .ifdef")
	} else {
		// pop
		seg = ifStack[len-1]
		ifStack = ifStack[:len-1]
	}
}

// bites haengt Ausdruecke an Objekt an
func bite(ex ...*expr) {
	for _, e := range ex {
		obj[seg] = append(obj[seg], byte(e.n))

		if e.n < -128 || 255 < e.n {
			warn(2, "%d exceeds byte size", e.n)
		}
	}
}

// words haengt Ausdruecke an Objekt an
func word(ex ...*expr) {
	if (seg+len(obj[seg]))&1 == 1 {
		warn(1, "word starts at odd address")
	}

	for _, e := range ex {
		obj[seg] = append(obj[seg], byte(e.n), byte(e.n>>8))

		if e.n < -32768 || 65535 < e.n {
			warn(2, "%d exceeds word size", e.n)
		}
	}
}

// spaces beschert das Objekt mit Plaetzchen
func space(e *expr) {
	// Ausdruck darf keine future ref enthalten, sonst Kreisabhaengigkeit:
	// .space bestimmt future ref, future ref bestimmt space
	if e.p > pos {
		warn(1, "expr depends on future symbol")
		warn(2, "expr depends on future symbol")
	}

	obj[seg] = append(obj[seg], make([]byte, e.n)...)
}

// asciiz unquotet String und haengt ihn samt 0 an segment an
func asciiz(s string) {
	s, err := strconv.Unquote("\"" + s + "\"")
	if err != nil {
		warn(1, err.Error())
	}
	obj[seg] = append(obj[seg], []byte(s)...)
	obj[seg] = append(obj[seg], byte(0))
}

// calc verrechnet 2 Ausdrucke und gibt Ergebnis zurueck
func calc(e1 *expr, op byte, e2 *expr) *expr {
	var e expr

	// die Position des Ergebnisses ist die groessere Position der Ausdruecke
	p := (map[bool]int{true: e1.p, false: e2.p})[e1.p > e2.p] // ?: in Go

	switch op {
	case '+':
		e = expr{n: e1.n + e2.n, p: p}
	case '-':
		e = expr{n: e1.n - e2.n, p: p}
	case '|':
		e = expr{n: e1.n | e2.n, p: p}
	case '^':
		e = expr{n: e1.n ^ e2.n, p: p}
	case '*':
		e = expr{n: e1.n * e2.n, p: p}
	case '/':
		e = expr{n: e1.n / e2.n, p: p}
	case '%':
		e = expr{n: e1.n % e2.n, p: p}
	case '&':
		e = expr{n: e1.n & e2.n, p: p}
	default:
		panic("STRANGE OPERATOR")
	}

	if e.n < -32768 || 65535 < e.n {
		warn(2, "result of '%c' exceeds word size (%#x)", op, e.n)
	}
	return &e
}

// number macht size check und gibt Ausdruck zurueck
func number(n int) *expr {
	e := expr{n: n, p: 0}        // Position 0, weil Zahlen a priori definiert
	if n < -32768 || 65535 < n { // 65535, damit 0x8000 kein Fehler ist
		warn(1, "%d exceeds word size", n) // Zahl schon im 1. Durchlauf fest
	}
	return &e
}

// symbol gibt Ausdruck zurueck
func symbol(s string) *expr {
	if e, ok := symTab[s]; ok { // Symbol schon definiert
		return &e
	} else { // Symbol noch nicht definiert
		warn(2, "'%s' is not defined", s)
		return &expr{p: int(^uint(0) >> 1)} // irgendwas ganz weit vorne
	}
}

// warn gibt formatierten String beim angegebenen Pass aus und setzt exit=1
func warn(p int, f string, a ...interface{}) {
	// um doppelte Fehlermeldungen zu vermeiden, diese nur bei 1 pass ausgeben
	// ausserdem Fehler in nicht zutreffenden defines ignorieren fuer so etwas:
	// .ifdef P3OUT\n	mov.b #0, &P3OUT\n	.endif
	if p != pass || seg < 0 {
		return
	}

	var param []interface{}

	// Dateiname und Zeile
	buf := bytes.NewBufferString("%s:%d: ")
	param = append(param, file, line)

	// Fehlermeldung
	buf.WriteString(f)
	buf.WriteString("\n")
	param = append(param, a...)

	fmt.Fprintf(os.Stderr, buf.String(), param...)
	ret = 1 // exit code
}

func punish(p int, f string, a ...interface{}) {
	warn(p, f, a)
	os.Exit(1)
}

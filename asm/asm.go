package asm

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	//"path"
	"sort"
)

var (
	// Parameter
	fs      = flag.NewFlagSet("fs", flag.ContinueOnError)
	symBool = fs.Bool("s", false, "show symbols")
	outFile = fs.String("o", "a.out", "output filename")
	format  = fs.String("t", "hex", "output format")

	// Assembler
	sources []struct { // keine map, u, Eingabereihenfolge zu behalten
		name    string
		content []byte
	}
	bakTab map[string]map[string]expr // Sicherung lokaler Symbole

	// Parser
	obj     map[int][]byte // seg -> bytes
	symTab  map[string]expr
	pass    int    // Durchlauf
	ret     int    // Rueckgabewert
	pos     int    // absolute "Zeilennummer" fuer Bestimmung von future refs
	file    string // Dateiname
	line    int    // Zeilennummer wird von eol-Regel inkrementiert
	seg     int    // aktuelles Segment
	ifStack []int  // Tiefe von .ifdef

	// Lexer
	src []byte // Eingabe des Lexers
)

func Main() int {
	// brauchen neben "$0 subcommand" noch Datei oder so
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "Usage: %s %s [options] files\n",
			os.Args[0], os.Args[1]) // Obermain stellt sicher, dass Args[0:2] ok
		fs.PrintDefaults()
	}
	fs.Parse(os.Args[2:])

	// Dateien einlesen
	for _, path := range fs.Args() {
		f, err := os.Open(path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			os.Exit(1)
		}
		defer f.Close()
		ret |= Read(f)
	}

	// wenn Fehler beim Lesen aufgetreten sind, dann aufhoeren
	if ret != 0 {
		os.Exit(ret)
	}

	// Datei fuer Ausgabe
	f, err := os.Create(*outFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	defer f.Close()

	ret = Write(f, *format)
	if ret != 0 {
		os.Exit(ret)
	}

	return 0
}

func Read(r io.Reader) int {
	// Eingaben brauchen Namen
	var name string
	switch r.(type) {
	case *os.File:
		name = r.(*os.File).Name()
	default:
		// keine Ahnung, was bei bei STDIN oder Strings als Namen verwenden soll
		name = fmt.Sprintf("%p", r)
	}

	// Dateiinhalte speichern, um dann zu verarbeiten
	content, err := ioutil.ReadAll(r)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return 1
	}
	sources = append(sources, struct {
		name    string
		content []byte
	}{
		name,
		content,
	})
	return 0
}

func Write(w io.Writer, format string) int {
	// erstmal gucken, ob's fuer das Ausgabeormat eine Funktion gibt
	fn, ok := map[string]func(io.Writer) int{
		"hex": hex,
		"txt": txt,
	}[format]

	if !ok {
		fmt.Fprintf(os.Stderr, "%s is not a supported format\n")
		return 1
	}

	symTab = make(map[string]expr)
	bakTab = make(map[string]map[string]expr)

	for i := 1; i < 3; i++ {
		pass = i                   // Durchlauf
		pos = 0                    // absolute "Zeilennummer", kann mit 0 anf.
		obj = make(map[int][]byte) // alte Sections entsorgen

		for _, s := range sources {
			file = s.name   // Dateiname fuer Fehlermeldungen
			line = 1        // Zeile fuer Fehlermeldungen
			src = s.content // Eingabe fuer Lexer
			seg = 0         // Segment zuruecksetzen

			// Sicherung lokaler Symbole einspielen
			for sym, e := range bakTab[file] {
				symTab[sym] = e
			}

			yyParse(&lexer{}) // los geht's

			// map fuer Sicherung lokaler Symbole fuer 2. Durchlauf
			if _, ok := bakTab[file]; !ok {
				bakTab[file] = make(map[string]expr)
			}

			// nach jeder Datei lokale Symbole loeschen
			for sym, e := range symTab {
				if len(sym) == 0 || sym[0] < 'A' || 'Z' < sym[0] {
					// lokale Symbole sichern, um future references
					// beim 2. Durchlauf zu kennen
					bakTab[file][sym] = e
					delete(symTab, sym)
				}
			}

			// Adresse 0 ist wahrscheinlich nicht im Sinne des Benutzers
			if _, ok := obj[0]; ok && i > 1 {
				fmt.Printf("%s: there is something at address 0\n", file)
			}
		}

		// wenn 1. Durchlauf nicht erfolgreich war, tut's der 2. auch nicht
		if ret != 0 {
			return ret
		}
	}
	sources = nil // bei Read mit sauberen Quellen anfangen

	// bei -s Symbole ausgeben
	if *symBool {
		for sym, e := range symTab {
			//if ok, err := path.Match(*symRe, sym); err == nil && ok {
			fmt.Printf("%s: %d\n", sym, e.n)
			//}
		}
	}

	// Objekt im gewuenschten Format ausgeben
	return fn(w)

}

// text gibt Object als TI Text aus
func txt(w io.Writer) int {
	// sections sortieren
	var sects []int
	for s := range obj {
		sects = append(sects, s)
	}
	sort.Ints(sects)

	for _, s := range sects {
		fmt.Fprintf(w, "@%04x\n%04x\n", s, obj[s])
	}

	return 0
}

// hex gibt Objekt als Intel HEX aus
func hex(w io.Writer) int {

	// sections sortieren
	var segms []int
	for s := range obj {
		if s < 0 {
			continue // negative segms (unerfuellte ifdefs) ignorieren
		}
		segms = append(segms, s)
	}
	sort.Ints(segms)

	// HEX bauen
	for _, s := range segms {
		seg := obj[s]

		for off, count := 0, 0; off < len(seg); off += count {
			count = len(seg[off:])
			if count > 16 {
				count = 16
			}
			addr := s + off
			if addr > 0xffff {
				// Adressfeld hat nur 2 Bytes
				fmt.Fprintf(os.Stderr, "binary exeeds 65535 bytes\n")
				return 1
			}

			fmt.Fprintf(w,
				":%02X%04X00%X%02X\n",
				count, addr, seg[off:off+count], func() int {
					// Quersumme
					cs := count + addr&0xff + addr>>8&0xff
					for _, b := range seg[off : off+count] {
						cs += int(b)
					}
					//return (cs ^ 0xff + 1) & 0xff
					return (^cs + 1) & 0xff
				}(),
			)
		}
	}
	fmt.Fprintf(w, ":00000001FF\n")

	return 0
}

func elf() ([]byte, error) {
	return nil, nil
}

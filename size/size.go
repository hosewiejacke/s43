// 2016-08-09

package size

// calculate size of Intel HEX binary

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
)

var (
	fs = flag.NewFlagSet("fs", flag.ContinueOnError)
)

func Main() int {
	fs.Parse(os.Args[2:])

	if fs.NArg() < 1 {
		fmt.Fprintf(os.Stderr, "Usage: %s %s [options] files\n",
			os.Args[0], os.Args[1])
		fs.PrintDefaults()
	}

	// ueber Dateinamen iterieren
	for _, path := range fs.Args() {
		f, err := os.Open(path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			os.Exit(1)
		}
		defer f.Close()

		var size int64 // weil strconv.ParseInt einen int64 zurueckgibt
		var line int   // Zeilen fuer Fehlerausgabe zaehlen

		// Datei einlesen
		sc := bufio.NewScanner(f)
		for sc.Scan() {
			line++

			text := sc.Text()
			if len(text) == 0 {
				continue // Leerzeile ist OK
			}

			// Nichtleere Zeile muss mit ':' anfangen
			if text[0] != ':' {
				fmt.Fprintf(os.Stderr,
					"%s:%d: unexpected %q\n", path, line, text[0])
				return 1
			}

			// Zeile sollte glaubwuerdige Laenge haben
			if len(text) < 3 { // ':' + len + addr
				fmt.Fprintf(os.Stderr, "%s:%d: record to short\n", path, line)
				return 1
			}

			// Recordlaenge parsen
			if n, err := strconv.ParseInt(text[1:3], 16, 64); err != nil {
				fmt.Fprintf(os.Stderr, "%s:%d: %v\n", path, line, err)
				return 1
			} else {
				size += n
			}
		}
		fmt.Printf("%10d\t%s\n", size, path)
	}

	return 0
}
